﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.WebServiceClient;
using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web.Mvc;

namespace Dynamics.Controllers
{
    [Authorize]
    public class CrmSdkController : Controller
    {

        private string clientId =
         ConfigurationManager.AppSettings["ida:ClientId"];
        private string authority =
         ConfigurationManager.AppSettings["ida:AADInstance"] + "common";
        private string aadInstance =
         ConfigurationManager.AppSettings["ida:AADInstance"];
        private string OrganizationHostName =
         ConfigurationManager.AppSettings["ida:OrganizationHostName"];
        private string appKey =
         ConfigurationManager.AppSettings["ida:ClientSecret"];


        // GET: CrmSdk
        public ActionResult Index()
        {
            string tenantID = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value;

            string organizationName = "harmonie";

            var resource = string.Format(OrganizationHostName, organizationName);

            ClientCredential clientcred = new ClientCredential(clientId, appKey);

            AuthenticationContext authenticationContext = new AuthenticationContext(aadInstance + tenantID);

            AuthenticationResult authenticationResult = authenticationContext.AcquireToken(resource, clientcred);

            var requestedToken = authenticationResult.AccessToken;

            //var url = GetServiceUrl(organizationName);

            var url = "https://harmonie.crm4.dynamics.com/api/data/v8.2/accounts";

            var client = new WebClient
            {
                Headers = new WebHeaderCollection
                {
                    {"Accept", "application/json;odata=verbose;charset=utf-8, */*"},
                    {"Content-Type", "application/json;odata=verbose;charset=utf-8"},
                    {"Authorization", $"Bearer {requestedToken}"},
                    {"OData-Version", "4.0" },
                    {"Prefer", "odata.maxpagesize=5000" },
                }
            };

            var result = client.DownloadString(url);

            return View((object)"asdasd");


            //using (var sdkService = new OrganizationWebProxyClient(GetServiceUrl(organizationName), false))
            //{
            //    sdkService.HeaderToken = requestedToken;
            //    OrganizationRequest request = new OrganizationRequest()
            //    {
            //        RequestName = "WhoAmI"
            //    };

            //    OrganizationResponse response = sdkService.Execute(request);
            //    return View((object)string.Join(",", response.Results.ToList()));
            //}
        }

        private Uri GetServiceUrl(string organizationName)
        {
            var organizationUrl = new Uri(
             string.Format(OrganizationHostName, organizationName)
             );
            return new Uri(
             organizationUrl +
             @"/xrmservices/2011/organization.svc/web?SdkClientVersion=8.2"
         );
        }
    }
}