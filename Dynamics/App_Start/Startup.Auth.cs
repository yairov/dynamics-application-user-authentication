﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.IdentityModel.Claims;

namespace Dynamics
{
    public partial class Startup
    {
        private static string clientId = ConfigurationManager.AppSettings["ida:ClientId"];
        private string appKey = ConfigurationManager.AppSettings["ida:ClientSecret"];
        private static string aadInstance = ConfigurationManager.AppSettings["ida:AADInstance"];
        private string authority = aadInstance + "common";
        private string OrganizationHostName = ConfigurationManager.AppSettings["ida:OrganizationHostName"];

        public void ConfigureAuth(IAppBuilder app)
        {
            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);

            app.UseCookieAuthentication(new CookieAuthenticationOptions { });

            app.UseOpenIdConnectAuthentication(
                new OpenIdConnectAuthenticationOptions
                {
                    ClientId = clientId,
                    Authority = authority,
                    TokenValidationParameters = new System.IdentityModel.Tokens.TokenValidationParameters
                    {
                        ValidateIssuer = false,
                    },
                    Notifications = new OpenIdConnectAuthenticationNotifications()
                    {
                        RedirectToIdentityProvider = (context) =>
                        {
                            string appBaseUrl = context.Request.Scheme + "://" + context.Request.Host + context.Request.PathBase + "/";

                            context.ProtocolMessage.RedirectUri = appBaseUrl;

                            context.ProtocolMessage.PostLogoutRedirectUri = appBaseUrl;

                            return Task.FromResult(0);
                        },
                        SecurityTokenValidated = (context) =>
                        {
                            string issuer = context.AuthenticationTicket.Identity.FindFirst("iss").Value;

                            string UPN = context.AuthenticationTicket.Identity.FindFirst(ClaimTypes.Name).Value;

                            string tenantID = context.AuthenticationTicket.Identity.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value;

                            return Task.FromResult(0);
                        },
                        AuthorizationCodeReceived = (context) =>
                        {
                            var code = context.Code;

                            ClientCredential credential = new ClientCredential(clientId, appKey);

                            string tenantID = context.AuthenticationTicket.Identity.FindFirst("http://schemas.microsoft.com/identity/claims/tenantid").Value;

                            var resource = string.Format(OrganizationHostName, '*');

                            Uri returnUri = new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path));

                            AuthenticationContext authContext = new AuthenticationContext(aadInstance + tenantID);

                            AuthenticationResult result = authContext.AcquireTokenByAuthorizationCode(
                                         code,
                                         new Uri(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path)),
                                         credential,
                                         resource);

                            return Task.FromResult(0);
                        },
                        AuthenticationFailed = (context) =>
                        {
                            context.OwinContext.Response.Redirect("/Home/Error");

                            context.HandleResponse();

                            return Task.FromResult(0);
                        }
                    }
                });

        }
    }

}
